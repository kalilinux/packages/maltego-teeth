# Maltego Teeth

This runs on Kali Linux

## Installation

Load the config file called /opt/Teeth/etc/Maltego_config.mtz file into Maltego.  This is painless:

1) Open Maltego
2) Click top left globe/sphere (Application button) 
3) Import -> Import configuration, choose /opt/Teeth/etc/Maltego_config.mtz

Install the python packages specified in the requirements.txt as root. 

## Usage

Transforms are included under the _Local Transforms_ in the Transform Context Menu.

For some Transforms (for example, Transforms that use Nmap), Maltego needs to be run as root.

## Notes

- Config file is in /opt/Teeth/etc/TeethConfig.txt. Everything can be set in the config file.

- Log file is /var/log/Teeth.log, tail -f it while you running transforms for
real time logs of what's happening.

- You can set DEBUG/INFO. DEBUG is useful for seeing progress - set in
/opt/Teeth/units/TeethLib.py line 26

- Look in cache/ directory. Here you'll find caches of:
  - Nmap results
  - Mirrors

- You need to remove cache files by hand if you no longer want them. You can run housekeep/clear_cache.sh but it 
removes EVERYTHING.

- In /housekeep is killswitch . sh - it's the same as killall python.


## Disclaimer

It is User’s exclusive responsibility to ensure that using Maltego Teeth conforms with applicable laws, in particular 
of countries in which Maltego Teeth is used. 
Maltego Teeth may not be used in the generation of unsolicited email (spam) or for unethical purposes.  
User warrants that: 
  (i) User is not a citizen, national, permanent resident of, or incorporated or organized to do business in, and is 
  not under the control of the government of any country to which the United States or European Union embargoes goods; 
  (ii) User is not included on any list of sanctioned or ineligible parties maintained by the United States or European 
  Union; 
  (iii) User will not sell, export, re-export, transfer, use, or enable the use of Maltego Teeth, its related technology 
  and services, or any other items that may be provided by Licensor, directly or indirectly:
  (a) to or for end-use in or by the countries listed in (i) above or any citizens, nationals or permanent residents of 
  such countries; (b) to or for end-use by any person or entity determined by any United States or European Union  agency 
  to be ineligible to receive exports, including but not limited to persons and entities designated on the lists described 
  in (ii) above; and (c) to or for end-uses prohibited by United States or European Union export or sanctions laws and 
  regulations.   
 
 
