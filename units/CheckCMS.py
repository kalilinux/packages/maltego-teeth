import socket

from MaltegoTransform import *
from TeethLib import *

def checkCMS(raw,target,findwords,value,entry,property,TRXi):
 logger.debug("Looking on [%s] for the words [%s]"%(target,findwords))
 try:
  html = urllib.request.urlopen(target,timeout=3).read()
#  logger.debug("Found [%s]"%html)
  if (html.find(findwords.encode())>0):
   logger.info("Found [%s] on [%s]"%(findwords,target))
   TitEnt=TRXi.addEntity("paterva.PossibleEntryPoint","")
   TitEnt.setValue(value)
   TitEnt.addAdditionalFields("entry","Entry","strict",entry)
   TitEnt.addAdditionalFields("type","Type","strict",property)
   TitEnt.setDisplayInformation("Click <a href='"+target+"'>here</a> to open")
   return TRXi
 except urllib.error.HTTPError as e:
   logger.debug(f"{e} was returned.")
 except:
   return TRXi

me = MaltegoTransform()
me.parseArguments(sys.argv);
target=sys.argv[1]

logger.info("Searching for known CMS on [%s]"%target)
#see that it resolves as urllib cannot set timeout on DNS
try:
  a=socket.gethostbyname(target)
except:
  logger.info("Could not resolve [%s] to IP address"%target)
  me.returnOutput()
  quit()

checkCMS(target,"http://"+target+"/wp-login.php","wordpress","WordPress CMS",target,"WP",me)
checkCMS(target,"http://"+target+"/site/administrator/index.php","Joomla","Joomla CMS",target,"JML",me)
checkCMS(target,"http://"+target+":2082/login/","cPanel","cPanel",target,"CP",me)
checkCMS(target,"http://"+target+"/user/","drupal","Drupal",target,"DP",me)

me.returnOutput()